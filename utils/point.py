class point():
    def __init__(self, coords: tuple = ()) -> 'point':
        self.__coords = tuple(coords)

    @property
    def coords(self): return self.__coords

    @property
    def adjacent(self):
        adj = [0] * len(self.coords)
        adj = [adj[:n] + [i] + adj[n+1:]
               for n, _ in enumerate(adj)
               for i in (1, -1)]

        return [self + n for n in adj]

    def __getitem__(self, index):
        try:
            return self.coords[index]
        except IndexError:
            return 0

    def __len__(self) -> int:
        return len(self.coords)

    def __add__(self, other: 'point' or tuple[int]) -> 'point':
        try:
            coords = other.coords
        except AttributeError:
            coords = tuple(other)

        c = self.coords
        coords += (len(self) - len(coords)) * (0,)
        c += (len(coords) - len(c)) * (0,)

        return type(self)([s+o for s, o in zip(c, coords)])

    def __radd__(self, other: 'point' or tuple[int]):
        return self + other

    def __eq__(self, other: object) -> bool:
        try:
            coords = other.coords
        except AttributeError:
            coords = other

        pad = abs(len(self) - len(coords)) * (0,)

        return (self.coords == coords + pad
                or self.coords + pad == coords)

    def __iter__(self):
        return iter(self.coords)

    def __hash__(self) -> int:
        return hash(self.coords)

    def __str__(self) -> str:
        return f'point{self.coords}'

    def __repr__(self) -> str:
        return str(self)
