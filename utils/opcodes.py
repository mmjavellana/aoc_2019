from collections import defaultdict


class machine:
    def __init__(self, state: list[int], inputs: list[int] = []):
        self.orig_state = defaultdict(int)
        self.orig_state.update({i: c for i, c in enumerate(state)})
        self.initialize(inputs)

    def initialize(self, inputs: list[int] = []) -> 'machine':
        self.state = self.orig_state.copy()
        self.inputs = inputs.copy()
        self.pointer = 0
        self.halted = False
        self.wait = False
        self.relative_base = 0

        return self

    def run(self):
        while not self.halted:
            current_pos = f"{self.state[self.pointer]:05}"
            opcode = int(current_pos[-2:])
            param_modes = current_pos[-3::-1]

            output = self._get_opcode(opcode, param_modes)()

            if output is not None:
                yield output

            if self.wait:
                self.wait = False
                break

    def add_input(self, input_: list[int] or int or str):
        match input_:
            case list():
                self.inputs += input_
            case str():
                self.inputs += [ord(i) for i in input_]
            case int():
                self.inputs.append(input_)
            case _:
                raise 'Invalid argument'

        return self

    def get_state(self):
        return self.state

    def is_halted(self):
        return self.halted

    def _get_opcode(self, code: int, modes: list):
        def param_index(i: int):
            index = self.state[self.pointer + i + 1]

            if modes[i] == '2':
                index += self.relative_base

            return index

        def get_value(i: int):
            index = param_index(i)
            param = self.state[index]

            if modes[i] == '1':
                param = index

            return param

        def set_value(i: int, value: int):
            assert modes[i] != '1'
            self.state[param_index(i)] = value

        def add():
            set_value(2, get_value(0) + get_value(1))
            self.pointer += 4

        def mult():
            set_value(2, get_value(0) * get_value(1))
            self.pointer += 4

        def input():
            if not self.inputs:
                self.wait = True
                return

            set_value(0, self.inputs.pop(0))
            self.pointer += 2

        def output():
            output = get_value(0)
            self.pointer += 2
            return output

        def jump_if_true():
            self.pointer = (get_value(1)
                            if get_value(0) != 0
                            else self.pointer + 3)

        def jump_if_false():
            self.pointer = (get_value(1)
                            if get_value(0) == 0
                            else self.pointer + 3)

        def less_than():
            set_value(2, get_value(0) < get_value(1))
            self.pointer += 4

        def equals():
            set_value(2, get_value(0) == get_value(1))
            self.pointer += 4

        def relative():
            self.relative_base += get_value(0)
            self.pointer += 2

        def halt():
            self.halted = True

        switcher = {
            1: add,
            2: mult,
            3: input,
            4: output,
            5: jump_if_true,
            6: jump_if_false,
            7: less_than,
            8: equals,
            9: relative,
            99: halt
        }

        return switcher.get(code, None)
