from days import aoc, day
from utils.decorators import time_it

day_number = 1


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.modules = [int(x) for x in input_data]
        pass

    @time_it
    def part1(self, input_data):
        return sum([calculate_fuel(x) for x in self.modules])

    @time_it
    def part2(self, input_data):
        fuel = [[calculate_fuel(x)] for x in self.modules]

        for x in fuel:
            while x[-1] > 0:
                x.append(calculate_fuel(x[-1]))

        return sum(sum(x) for x in fuel)


def calculate_fuel(module: int) -> "int":
    fuel = module // 3 - 2
    return fuel if fuel > 0 else 0


if __name__ == "__main__":
    DaySolution(day_number).run()
