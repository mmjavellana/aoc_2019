from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 9


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        return next(machine(self.state)
                    .add_input(1)
                    .run())

    @time_it
    def part2(self, input_data):
        return next(machine(self.state)
                    .add_input(2)
                    .run())


if __name__ == "__main__":
    DaySolution(day_number).run()
