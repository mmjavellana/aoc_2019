from days import aoc, day
from utils.decorators import time_it
from collections import deque
from collections.abc import Iterator
import heapq

day_number = 18


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.map = {x+y*1j: v
                    for y, r in enumerate(input_data)
                    for x, v in enumerate(r)
                    if v != '#'}
        self.doors = {v: k for k, v in self.map.items() if v.isupper()}
        self.keys = {v: k for k, v in self.map.items() if v.islower()}
        self.start = next(k for k, v in self.map.items() if v == '@')

    @time_it
    def part1(self, input_data):
        return bfs(self.map, [self.start], sorted(self.keys))

    @time_it
    def part2(self, input_data):
        map_ = self.map
        start = [self.start + d for d in [1+1j, 1-1j, -1+1j, -1-1j]]

        for d in [0, 1, -1, 1j, -1j]:
            map_.pop(self.start + d, None)

        for d in start:
            map_[d] = '@'

        return bfs(map_, start, sorted(self.keys))


def bfs(map_: dict, start: list, keys: dict) -> tuple[list, int]:
    map_ = map_.copy()
    start = [(i.real, i.imag) for i in start]
    queue = [(0, start, '')]
    seen = {}

    while queue:
        dist, coords, curr_keys = heapq.heappop(queue)

        for i, (x, y) in enumerate(coords):
            pos = x+y*1j

            if len(curr_keys) == len(keys):
                return dist, curr_keys

            for d, p, k in find_next_keys(map_, pos, dist, curr_keys, seen):
                new_coords = coords[:]
                new_coords[i] = (p.real, p.imag)
                heapq.heappush(queue, (d, new_coords, k))


def find_next_keys(
            map_: dict, start: complex, dist: int, keys: str, seen: dict
        ) -> Iterator[tuple[int, str]]:
    dirs = [1, -1, 1j, -1j]
    queue = deque([(dist, start)])
    sorted_keys = ''.join(sorted(keys))

    while queue:
        dist, pos = queue.popleft()

        if seen.get((pos, sorted_keys), float('inf')) <= dist:
            continue

        seen[pos, sorted_keys] = dist

        dist += 1
        for npos in (pos + d for d in dirs):
            if npos not in map_:
                continue

            curr_pos = map_[npos]
            if curr_pos.islower() and curr_pos not in keys:
                yield dist, npos, keys + curr_pos
                continue
            elif curr_pos.isupper() and curr_pos.lower() not in keys:
                continue

            queue.append((dist, npos))


if __name__ == "__main__":
    DaySolution(day_number).run()
