from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 21


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(i) for i in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        bot = SpringBot(self.state)

        bot.add_instruction('NOT A J')
        bot.add_instruction('NOT B T')
        bot.add_instruction('OR T J')
        bot.add_instruction('NOT C T')
        bot.add_instruction('OR T J')
        bot.add_instruction('AND D J')

        result = bot.walk()
        self.print_result(result)

        return result[-1]

    @time_it
    def part2(self, input_data):
        bot = SpringBot(self.state)

        bot.add_instruction('NOT A J')
        bot.add_instruction('NOT B T')
        bot.add_instruction('OR T J')
        bot.add_instruction('NOT C T')
        bot.add_instruction('OR T J')
        bot.add_instruction('AND D J')

        bot.add_instruction('AND E T')
        bot.add_instruction('OR H T')
        bot.add_instruction('AND T J')

        result = bot.run()
        self.print_result(result)

        return result[-1]

    def print_result(self, result: list[int]):
        with open(self.output_filename, 'w+') as f:
            f.writelines(chr(i) if i < 0x10FFFF else str(i) for i in result)


class SpringBot(machine):
    def result(self) -> list[int]:
        return list(super().run())

    def walk(self):
        self.add_instruction('WALK')
        return self.result()

    def run(self):
        self.add_instruction('RUN')
        return self.result()

    def add_instruction(self, instruction: str):
        self.add_input(instruction.upper() + '\n')


if __name__ == "__main__":
    DaySolution(day_number).run()
