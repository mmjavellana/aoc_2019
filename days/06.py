from days import aoc, day
from utils.decorators import time_it

day_number = 6


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        def parse_orbits(orbits: dict, obj: str):
            if obj not in orbits:
                return [obj]

            return [obj] + parse_orbits(orbits, orbits[obj])

        orbits = {orbit: planet
                  for s in input_data
                  for planet, orbit in [s.split(')')]}

        self.paths = {o: parse_orbits(orbits, orbits[o])
                      for o in orbits}

    @time_it
    def part1(self, input_data):
        return sum(len(s) for s in self.paths.values())

    @time_it
    def part2(self, input_data):
        return len(set(self.paths["SAN"]) ^ set(self.paths["YOU"]))


if __name__ == "__main__":
    DaySolution(day_number).run()
