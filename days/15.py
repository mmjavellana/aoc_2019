from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 15


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        paths, _, dest = build_map(self.state[:])

        return len(paths[dest])

    @time_it
    def part2(self, input_data):
        paths, points, dest = build_map(self.state[:])

        dirs = [-1j, 1j, -1, 1]
        stack = [dest]
        paths = {dest: []}
        seen = set()

        while stack:
            coord = stack.pop()
            seen.add(coord)

            for d in dirs:
                new_coord = coord + d

                if new_coord in seen:
                    continue

                if new_coord in points:
                    stack.append(new_coord)
                    paths[new_coord] = paths[coord] + [coord]

        return max(len(i) for i in paths.values())


def build_map(state, start=0j):
    dirs = [-1j, 1j, -1, 1]
    stack = [(start, machine(state))]
    dest = None
    visited = set([start])
    paths = {start: []}

    while stack:
        coord, droid = stack.pop()

        for i, d in enumerate(dirs, start=1):
            next_coord = coord + d

            if next_coord in visited:
                continue

            new_droid = machine([v for v in droid.get_state().values()])
            new_droid.pointer = droid.pointer
            result = next(new_droid.add_input(i).run())

            if not result:
                continue

            if result == 2:
                dest = next_coord

            stack.append((next_coord, new_droid))
            paths[next_coord] = paths[coord] + [coord]

        visited.add(coord)

    return paths, visited, dest


if __name__ == "__main__":
    DaySolution(day_number).run()
