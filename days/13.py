from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine
from collections import defaultdict

day_number = 13


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        items = defaultdict(list)
        cabinet = machine(self.state)

        for x, y, t in group_iter(cabinet.run(), 3):
            items[t].append((x, y))

        return len(items[2])

    @time_it
    def part2(self, input_data):
        def cmp(a: int, b: int):
            return (a > b) - (a < b)

        screen = defaultdict(int)
        state = self.state[:]
        state[0] = 2
        cabinet = machine(state)

        while not cabinet.is_halted():
            for x, y, t in group_iter(cabinet.run(), 3):
                screen[x, y] = t

                if t == 4:
                    ball = x
                if t == 3:
                    paddle = x

            cabinet.add_input(cmp(ball, paddle))

        return screen[-1, 0]


def group_iter(list, count):
    return zip(*[iter(list)]*count)


if __name__ == "__main__":
    DaySolution(day_number).run()
