from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 19


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(i) for i in input_data[0].split(',')]
        self.machine = machine(self.state)

    @time_it
    def part1(self, input_data):
        size = 50
        return sum(self.check_beam(x, y)
                   for y in range(size)
                   for x in range(size))

    @time_it
    def part2(self, input_data):
        size = 100 - 1
        x = y = 0

        while not self.check_beam(x + size, y):
            y += 1
            while not self.check_beam(x, y + size):
                x += 1

        return x, y, x*10_000 + y

    def check_beam(self, x, y):
        return next(self.machine.initialize([x, y]).run())


if __name__ == "__main__":
    DaySolution(day_number).run()
