from days import aoc, day
from utils.decorators import time_it

day_number = 8


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.width, self.height = (25, 6)
        length = self.width * self.height
        self.layers = [input_data[0][i:i+length]
                       for i in range(0, len(input_data[0]), length)]

    @time_it
    def part1(self, input_data):
        line = min(self.layers, key=lambda x: x.count('0'))
        return line.count('1') * line.count('2')

    @time_it
    def part2(self, input_data):
        image = ['2'] * (self.width * self.height)
        for layer in self.layers:
            for i, (p1, p2) in enumerate(zip(layer, image)):
                image[i] = p2 if p2 != '2' else p1

        for line in [''.join(image[i:i+self.width])
                     for i in range(0, len(image), self.width)]:
            print(line.replace('0', ' ').replace('1', '*'))

        return 'See print out above'


if __name__ == "__main__":
    DaySolution(day_number).run()
