from days import aoc, day
from itertools import product
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 2


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        return compute(12, 2, self.state[:])

    @time_it
    def part2(self, input_data):
        target = 19690720

        for x, y in product(range(99), range(99)):
            test = compute(x, y, self.state[:])
            if test == target:
                return x * 100 + y


def compute(input1: int, input2: int, state: list) -> "int":
    state[1] = input1
    state[2] = input2

    computer = machine(state)

    while not computer.is_halted():
        next(computer.run(), None)

    return computer.get_state()[0]


if __name__ == "__main__":
    DaySolution(day_number).run()
