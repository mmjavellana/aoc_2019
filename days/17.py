from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 17


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        map_ = generate_map(self.state)
        neighbours = [1, 1j, -1, -1j]

        scaffolds = [k for k, v in map_.items() if v == '#']
        intersections = [i for i in scaffolds
                         if all(i + n in scaffolds for n in neighbours)]

        return sum(int(i.real * i.imag) for i in intersections)

    @time_it
    def part2(self, input_data):
        map_ = generate_map(self.state)
        path = get_path(map_)
        inputs = compress_path(path)

        state = self.state[:]
        state[0] = 2

        robot = machine(state, inputs)

        return list(robot.run())[-1]


def print_map(map_):
    coords = [(int(i.real), int(i.imag)) for i in map_]
    max_bound = tuple(map(max, *coords))

    for y in range(max_bound[1] + 1):
        line = ' '
        for x in range(max_bound[0] + 1):
            line += map_[x+y*1j]
        print(line)


def generate_map(state):
    robot = machine(state)
    map_ = {}

    coord = 0j
    for i in robot.run():
        if i == 10:
            coord += 1j - coord.real
            continue

        map_[coord] = chr(i)
        coord += 1
    return map_


def get_path(map_):
    curr_pos = next((k for k, v in map_.items() if v in '<>^v'))
    scaffolds = [k for k, v in map_.items() if v == '#'] + [curr_pos]
    directions = list('^>v<')
    dir_ = map_[curr_pos]

    turns = {'^': [-1, 1], 'v': [1, -1], '>': [-1j, 1j], '<': [1j, -1j]}

    path = []

    while True:
        lr = turns[dir_]
        turn = lr[0] if curr_pos + lr[0] in scaffolds else lr[1]

        x = directions.index(dir_)
        y = x + (lr.index(turn) * 2 - 1)

        dir_ = directions[y % 4]

        count = 0
        while curr_pos + turn in scaffolds:
            count += 1
            curr_pos += turn
            map_[curr_pos] = dir_

        if count == 0:
            break

        path += [f'{"R" if y > x else "L"},{count},']
    return path


def compress_path(path):
    uncompressed = ''.join(path)
    compressed = uncompressed[:]

    prog = ord('A')
    i = 1
    x = ''
    while uncompressed:
        while uncompressed[:i] in uncompressed[i:]:
            i += 1

        p = uncompressed[:i-1]

        while '|' in p:
            p = p[:-1]

        uncompressed = uncompressed.replace(p, '|')

        p = p[:-1]
        compressed = compressed.replace(p, chr(prog))
        x += p + '\n'
        prog += 1

        while uncompressed.startswith("|"):
            uncompressed = uncompressed[1:]

        i = 1

    compressed = compressed[:-1]
    x = compressed + '\n' + x + 'n\n'
    inputs = [ord(i) for i in x]

    return inputs


if __name__ == "__main__":
    DaySolution(day_number).run()
