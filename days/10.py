from days import aoc, day
from utils.decorators import time_it
from math import gcd, atan2

day_number = 10


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.asteroids = [(x, y)
                          for y, line in enumerate(input_data)
                          for x, pos in enumerate(line)
                          if pos == "#"]

        self.station = (0, 0)

    @time_it
    def part1(self, input_data):
        best = max((len(get_visible(self.asteroids, pos)), pos)
                   for pos in self.asteroids)
        self.station = best[1]
        return best[1], best[0]

    @time_it
    def part2(self, input_data):
        asteroids = self.asteroids[:]
        removed = []

        while True:
            visible = get_visible(asteroids, self.station)
            if not visible:
                break
            for vector in sort_points(visible):
                point = tuple(map(sum, zip(self.station, vector)))

                while point not in asteroids:
                    point = tuple(map(sum, zip(point, vector)))

                asteroids.remove(point)
                removed.append(point)

        point = removed[199]

        return point[0] * 100 + point[1]


def sort_points(points):
    points = sorted(points,
                    key=lambda x: atan2(x[1], x[0]))

    p = next((i for (i, e) in enumerate(points) if e[0] >= 0), None)

    if p:
        points = points[p:] + points[:p+1]

    return points


def get_visible(asteroids: list, point: tuple):
    (x, y) = point
    visible = set()

    for x1, y1 in asteroids:
        if (x, y) == (x1, y1):
            continue

        dx = x1 - x
        dy = y1 - y
        div = abs(gcd(dx, dy))
        dx /= div
        dy /= div

        visible.add((int(dx), int(dy)))

    return visible


if __name__ == "__main__":
    DaySolution(day_number).run()
