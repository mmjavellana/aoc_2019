from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 25


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(i) for i in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        bot = robot(self.state)
        while True:
            match input("'play', 'solve', or anything else to quit? ").lower():
                case 'play':
                    return bot.run()
                case 'solve':
                    # astronaut ice cream, easter egg,
                    # dark matter, weather machine
                    return 285213704
                case _:
                    return

    @time_it
    def part2(self, input_data):
        pass


class robot(machine):
    def __init__(self, state):
        super().__init__(state)
        self.last_state = None
        self.current_room = None
        self.buffer = []
        self.rooms = {}
        self.current_room = None

    def run(self):
        self.clear_terminal()
        while not self.halted:
            self.read_lines()

            if self.halted:
                if any('Santa' in line for line in self.buffer):
                    self.flush()
                    return
                else:
                    self.buffer.append('\nThat didn\'t work.')
                    self.buffer.append('\nundo or exit?')
            elif 'Unrecognized command.' in self.buffer:
                self.buffer.insert(-1, 'Valid commands: '
                                   '(n, s, e, w, take {x},'
                                   ' drop {x}, look)')
            elif any('take' in line for line in self.buffer):
                item = '-' + self.buffer[1][12:-1]
                self.rooms[self.current_room]['Items'].remove(item)

            elif any('drop' in line for line in self.buffer):
                item = '-' + self.buffer[1][12:-1]
                self.rooms[self.current_room]['Items'].append(item)

            self.parse_room()
            self.get_input()

    def read_lines(self):
        line = ''
        lines = 0
        for i in super().run():
            if lines > 50:
                self.halted = True
                return
            if i == 10:
                lines += 1
                self.buffer.append(line)
                line = ''
            else:
                line += chr(i)

    def command(self, command):
        if command.lower() == 'exit':
            self.halted = True
        elif self.halted:
            if command == 'undo':
                print("Undoing last move")
                self.undo()
                self.read_lines()
                self.look()
        elif command == 'look':
            self.look()
        else:
            self.save()
            self.add_input(command + '\n')

    def get_input(self, prompt='Command?'):
        if self.buffer and self.buffer[-1]:
            prompt = self.buffer.pop()
        self.flush()
        self.command(input(prompt + ' '))

    def flush(self):
        print('\n'.join(self.buffer))
        self.buffer.clear()

    def save(self):
        self.last_state = (
            self.state.copy(),
            self.pointer,
            self.relative_base
        )

    def undo(self):
        self.state = self.last_state[0]
        self.pointer = self.last_state[1]
        self.relative_base = self.last_state[2]
        self.inputs.clear()
        self.halted = False
        self.wait = True

    def look(self):
        room = self.rooms[self.current_room]
        lines = []

        lines.append('\n\n')
        lines.append(self.current_room)
        lines.append(room['description'] + '\n')

        if 'Doors' in room and room['Doors']:
            lines.append('Doors here lead:')
            lines.extend(room['Doors'])
            lines.append('')

        if 'Items' in room and room['Items']:
            lines.append('Items here:')
            lines.extend(room['Items'])
            lines.append('')

        self.buffer.extend(lines)
        self.get_input()

    def parse_room(self):
        lines = [s for s in reversed(self.buffer) if s]

        if '==' in lines[-1]:
            room = lines.pop()
            contents = {'description': lines.pop(), 'Items': [], 'Doors': []}
            key = None

            for line in reversed(lines[1:]):
                if line[0] == '-':
                    contents[key].append(line)
                else:
                    key = line.split()[0]

            self.rooms[room] = contents
            self.current_room = room

    @staticmethod
    def clear_terminal():
        print(chr(27) + "[2J")


if __name__ == "__main__":
    DaySolution(day_number).run()
