from days import aoc, day
from utils.decorators import time_it
from math import prod, gcd
import re

day_number = 12


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.moons = [tuple(map(int, re.findall(r'-?\d+', l)))
                      for l in input_data]

    @time_it
    def part1(self, input_data):
        moons = self.moons[:]
        velocities = [[0] * len(m) for m in moons]

        for _ in range(1000):
            moons, velocities = step(moons, velocities)

        potential = map(sum, [map(abs, i) for i in moons])
        kinetic = map(sum, [map(abs, i) for i in velocities])

        return sum(map(prod, zip(potential, kinetic)))

    @time_it
    def part2(self, input_data):
        def lcm(x, y):
            return (x * y) // gcd(x, y)

        moons = self.moons[:]
        velocities = [[0] * len(m) for m in moons]
        base_axes = list(zip(*moons))
        cycle = [None] * len(base_axes)
        count = 0
        ans = 1

        while not all(cycle):
            count += 1
            moons, velocities = step(moons, velocities)

            v_axes = list(zip(*velocities))

            for i, x in enumerate(v_axes):
                if (not cycle[i]
                        and not any(x)):
                    cycle[i] = count
                    ans = lcm(ans, count)

        return 2 * ans


def step(moons: list, velocities: list):
    axes = list(zip(*moons))

    for i, moon in enumerate(moons):
        v = tuple(sum(1 if p > moon[i] else -1 if p < moon[i] else 0
                      for p in x)
                  for i, x in enumerate(axes))

        velocities[i] = tuple(map(sum, zip(velocities[i], v)))

    moons = [tuple(map(sum, zip(*x))) for x in zip(moons, velocities)]

    return moons, velocities


if __name__ == "__main__":
    DaySolution(day_number).run()
