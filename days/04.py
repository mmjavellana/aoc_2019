from days import aoc, day
from utils.decorators import time_it
from itertools import groupby

day_number = 4


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        min_code, max_code = map(int, input_data[0].split("-"))
        possible_codes = (str(i) for i in range(min_code, max_code + 1))
        self.codes = [s for s in possible_codes
                      if len(s) == 6
                      if any(i == j for i, j in zip(s, s[1:]))
                      if all(i <= j for i, j in zip(s, s[1:]))]

    @time_it
    def part1(self, input_data):
        return len(self.codes)

    @time_it
    def part2(self, input_data):
        codes = [s for s in self.codes
                 if any(sum(1 for _ in g) == 2 for _, g in groupby(s))]

        return len(codes)


if __name__ == "__main__":
    DaySolution(day_number).run()
