from days import aoc, day
from utils.decorators import time_it
from collections import defaultdict

day_number = 3


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.wires = [[(s[0], int(s[1:]))
                       for s in w.split(',')]
                      for w in input_data]

    @time_it
    def part1(self, input_data):
        paths, _ = get_paths(self.wires)

        intersections = [(abs(coord.real) + abs(coord.imag), coord)
                         for coord, wires in paths.items()
                         if len(wires) > 1]

        return min(intersections, key=lambda x: x[0])

    @time_it
    def part2(self, input_data):
        paths, scores = get_paths(self.wires)

        intersections = [(scores[coord], coord)
                         for coord, wires in paths.items()
                         if len(wires) > 1]

        return min(intersections, key=lambda x: x[0])


def get_paths(wires: list) -> dict:
    paths = defaultdict(set)
    scores = defaultdict(lambda: defaultdict(int))

    for i, wire in enumerate(wires):
        position = 0
        step = 0
        for vector, distance in wire:
            direction = get_direction(vector)

            while distance:
                step += 1
                position += direction

                paths[position].add(i)
                curr_score = scores[position][i]
                score = step if curr_score == 0 else min(step, curr_score)
                scores[position][i] = score

                distance -= 1

    return paths, {coord: sum(values.values())
                   for coord, values in scores.items()}


def get_direction(argument):
    switcher = {
        'U': -1,
        'D': 1,
        'L': -1j,
        'R': 1j,
    }

    return switcher.get(argument, 0)


if __name__ == "__main__":
    DaySolution(day_number).run()
