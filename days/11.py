from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine
from collections import defaultdict

day_number = 11


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        hull = robot(self.state).run(defaultdict(int))
        return len(hull)

    @time_it
    def part2(self, input_data):
        hull = defaultdict(int)
        hull[0+0j] = 1

        hull = robot(self.state).run(hull)

        coords = [(int(i.real), int(i.imag)) for i in hull]
        bounds = tuple(map(min, *coords)), tuple(map(max, *coords))

        for y in range(bounds[0][1] - 1, bounds[1][1] + 2):
            line = ''
            for x in range(bounds[0][0] - 1, bounds[1][0] + 2):
                line += '#' if hull[x + y*1j] else ' '

            print(line)

        return len(hull), bounds


class robot:
    def __init__(self, state: list):
        self._machine = machine(state)
        self.direction = -1j
        self.pos = 0

    def run(self, hull: dict):
        hull = hull.copy()

        while not self._machine.is_halted():
            (color, turn) = self._machine.add_input(hull[self.pos]).run()

            hull[self.pos] = color
            self.direction *= 1j if turn else -1j
            self.pos += self.direction

        return hull


if __name__ == "__main__":
    DaySolution(day_number).run()
