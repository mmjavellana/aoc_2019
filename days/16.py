from days import aoc, day
from math import prod
from utils.decorators import time_it

day_number = 16


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.signal = [int(i) for i in input_data[0]]

    @time_it
    def part1(self, input_data):
        phase = calculate_phase(self.signal)
        return ''.join([str(i) for i in phase[:8]])

    @time_it
    def part2(self, input_data):
        offset = int(''.join([str(i) for i in self.signal[:7]]))
        phase = calculate_phase(self.signal*10_000, offset)
        return ''.join([str(i) for i in phase[:8]])


def calculate_phase(signal, offset=0, count=100):
    cycle = [1, 0, -1, 0]
    phase = signal[offset:]
    length = len(phase) + offset

    for _ in range(count):
        new_phase = phase[:]
        for i in range(length - offset, 0, -1):
            digit = 0
            if i + offset > length // 2:
                digit = sum(new_phase[i-1:i+1])
            else:
                pattern = [[cycle[x % len(cycle)]] * i
                           for x in range(0, length//i + 1)]
                pattern = [i for i in pattern for i in i]
                digit = sum(list(map(prod, zip(phase[i-1:], pattern))))
                digit = abs(digit)
            new_phase[i-1] = digit % 10

        phase = new_phase

    return phase


if __name__ == "__main__":
    DaySolution(day_number).run()
