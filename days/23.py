from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 23


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(i) for i in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        bots = {i: machine(self.state, [i, -1]) for i in range(50)}

        while True:
            for bot in bots.values():
                for c, x, y in zip(*[bot.run()]*3):
                    if c == 255:
                        return x, y
                    bots[c].add_input([x, y])

    @time_it
    def part2(self, input_data):
        bots = {i: machine(self.state, [i, -1]) for i in range(50)}
        nat_sent = []
        nat = []

        while True:
            packets = 0
            for bot in bots.values():
                for c, x, y in zip(*[bot.run()]*3):
                    if c == 255:
                        nat = [x, y]
                    else:
                        packets += 1
                        bots[c].add_input([x, y])

            if not packets:
                bots[0].add_input(nat)
                if nat[1] in nat_sent:
                    return nat[1]
                nat_sent.append(nat[1])


if __name__ == "__main__":
    DaySolution(day_number).run()
