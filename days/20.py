from days import aoc, day
from utils.decorators import time_it
from collections.abc import Iterator
from collections import deque
import heapq
import re

day_number = 20


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        pass

    @time_it
    def part1(self, input_data):
        return Maze(input_data).shortest_path()

    @time_it
    def part2(self, input_data):
        return Maze(input_data).shortest_path(True)


class Maze():
    def __init__(self, map_: list[str]) -> 'Maze':
        self.__map = map_[:]
        self.__map_portals()

    def __map_portals(self):
        map_t = [''.join(i) for i in zip(*self.__map)]
        portals = [(0, y, i) for y, i in enumerate(self.__map)]
        portals += [(x, 0, i) for x, i in enumerate(map_t)]
        portals = [(x if x else d, y if y else d, p)
                   for x, y, i in portals
                   for m in re.finditer(r'(\w\w)(\.)?', i)
                   for p, d in [(m[1], m.start() + (2 if m[2] else -1))]]

        portals_ = {}
        for x, y, p in portals:
            portals_.setdefault(p, []).append((x, y))

        self.__start = portals_.pop('AA')[0]
        self.__target = portals_.pop('ZZ')[0]

        min_c = (3, 3)
        max_c = (len(self.__map[0]) - 3, len(self.__map) - 3)

        def direction(c: tuple):
            inner = all(i < j < k for i, j, k in zip(min_c, c, max_c))
            return 1 if inner else -1

        self.__portals = {}
        for p, c in portals_.items():
            self.__portals[c[0]] = p, c[1], direction(c[0])
            self.__portals[c[1]] = p, c[0], direction(c[1])

    def shortest_path(self, recurse: bool = False) -> int:
        queue = [(0, 0, self.__start, 'AA')]
        seen = {}
        cache = self.get_portal_paths()

        while queue:
            dist, level, (x, y), portals = heapq.heappop(queue)
            seen[x, y, level] = dist

            if ((x, y), level) == (self.__target, 0):
                return dist, portals

            for (p, (x, y), dl), nd in cache.get((x, y), []):
                if (x, y) == self.__target and level:
                    continue

                dl = dl if recurse else 0
                nl = level + dl

                if (nl < 0
                        or nl > len(self.__map)
                        or seen.get((x, y, nl), float('inf')) <= dist + nd):
                    continue

                p = portals + ['=', '+', '-'][dl] + p
                heapq.heappush(queue, (dist + nd, nl, (x, y), p))

    def get_portal_paths(self) -> Iterator[str, (int, int), int]:
        cache = {}
        dirs = [(1, 0), (0, 1), (-1, 0), (0, -1)]
        starts = [self.__start] + [k for k in self.__portals]

        for c in starts:
            queue = deque([(0, c)])
            cache[c] = set()
            seen = set()

            while queue:
                dist, (x, y) = queue.popleft()
                seen.add((x, y))

                dist += 1
                for nx, ny in ((x + i, y + j) for i, j in dirs):
                    if (self.__map[ny][nx] != '.'
                            or (nx, ny) in seen):
                        continue

                    if (nx, ny) == self.__target:
                        cache[c].add((('ZZ', (nx, ny), 0), dist))
                        continue

                    if (nx, ny) in self.__portals:
                        cache[c].add((self.__portals[nx, ny], dist + 1))
                        continue

                    queue.append((dist, (nx, ny)))

        return cache


if __name__ == "__main__":
    DaySolution(day_number).run()
