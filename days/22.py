from days import aoc, day
from utils.decorators import time_it

day_number = 22


# see https://codeforces.com/blog/entry/72593
# for explanation of algorithms used here
@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.commands = [(s[0], s[-1])
                         for i in input_data
                         for s in [i.split()]]

    @time_it
    def part1(self, input_data):
        deck = 10_007
        card = 2019
        mult, const = self.get_coefficients(deck)
        return (card * mult + const) % deck

    @time_it
    def part2(self, input_data):
        deck = 119315717514047
        shuffle = 101741582076661
        pos = 2020
        mult, const = self.get_coefficients(deck)

        def inverse(n, mod): return pow(n, mod - 2, mod)

        m = pow(mult, shuffle, deck)
        c = (const * (m - 1) * inverse(mult - 1, deck)) % deck
        mult, const = m, c

        return ((pos - const) * inverse(mult, deck)) % deck

    def get_coefficients(self, mod: int) -> tuple[int, int]:
        multiplier = 1
        constant = 0

        for command, i in self.commands:
            if i == 'stack':
                mult = const = -1
            elif command == 'cut':
                mult = 1
                const = -int(i)
            elif command == 'deal':
                mult = int(i)
                const = 0

            constant = (mult * constant + const) % mod
            multiplier = multiplier * mult % mod

        return multiplier, constant


if __name__ == "__main__":
    DaySolution(day_number).run()
