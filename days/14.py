from days import aoc, day
from utils.decorators import time_it
from math import ceil
import re

day_number = 14


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.reactions = {}
        self.coeff = {}
        p = re.compile(r"(\d+) ([A-Z]+)")

        for m in (p.findall(line) for line in input_data):
            self.reactions[m[-1][1]] = {
                'quantity': int(m[-1][0]),
                'ingredients': {k: int(v) for v, k in m[:-1]}
            }

        distance = {'ORE': 0}

        while len(distance) < len(self.reactions):
            for k in self.reactions:
                if k in distance:
                    continue

                ingredients = self.reactions[k]['ingredients']
                if not all(i in distance for i in ingredients):
                    continue

                distance[k] = max(distance[i] for i in ingredients) + 1
                self.reactions[k]['distance'] = distance[k]

    @time_it
    def part1(self, input_data):
        return calculate_ore_per_fuel(self.reactions)

    @time_it
    def part2(self, input_data):
        ore = 1_000_000_000_000
        fuel_min, fuel_max = 1, ore

        while fuel_max - fuel_min > 1:
            mid = (fuel_max + fuel_min) // 2
            qty = calculate_ore_per_fuel(self.reactions, mid)
            if qty <= ore:
                fuel_min = mid
            else:
                fuel_max = mid

        return fuel_min


def calculate_ore_per_fuel(reactions: dict, fuel: int = 1):
    needed = {'FUEL': fuel}
    reactions['ORE'] = {'quantity': 1, 'ingredients': {}, 'distance': 0}

    while 'ORE' not in needed or len(needed) > 1:
        element = max(needed, key=lambda x: reactions[x]['distance'])
        need = needed[element]
        del needed[element]

        base_quantity, ingredients, _ = reactions[element].values()

        for material, amount in ingredients.items():
            if material not in needed:
                needed[material] = 0

            needed[material] += ceil(need / base_quantity) * amount

    return needed['ORE']


if __name__ == "__main__":
    DaySolution(day_number).run()
