from days import aoc, day
from utils.decorators import time_it
from utils.point import point
from collections import defaultdict

day_number = 24
isdebug = False


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.world = [1 if v == '#' else 0
                      for i in input_data
                      for v in i]

    @time_it
    def part1(self, input_data):
        world = self.world[:]
        cache = [world]
        size = 5

        print_world(world, size, self.output_filename, heading='Initial State')
        rounds = 0
        while True:
            world = [has_bug(b, adjacent(world, i, size))
                     for i, b in enumerate(world)]

            rounds += 1
            print_world(world, size, self.output_filename,
                        heading=f'Round {rounds}', append=True)

            if world in cache:
                break

            cache.append(world)

        return rounds, sum(pow(2, i) * v for i, v in enumerate(world))

    @time_it
    def part2(self, input_data):
        size = 5
        inner, outer = get_edge_maps(size)
        middle = size * 2 + size//2
        empty = [0] * size * size
        world = defaultdict(lambda: empty[:], {0: self.world[:]})

        print_world(world[0], size, self.output_filename,
                    heading='Initial State')

        min_d = max_d = 0
        for rounds in range(200):
            nw = defaultdict(lambda: empty[:])
            for d in range(min_d - 1, max_d + 2):
                for i, b in enumerate(world[d]):
                    if i == middle:
                        continue
                    adj = adjacent(world[d], i, size)
                    adj += sum(world[d+1][n] for n in inner.get(i, []))
                    adj += sum(world[d-1][n] for n in outer.get(i, []))
                    nw[d][i] = has_bug(b, adj)

            min_d -= any(b for b in nw[min_d-1])
            max_d += any(b for b in nw[max_d+1])
            world = nw

            if (rounds+1) % 50 == 0 and isdebug:
                minute = f'Minute {rounds+1}\n'
                for d in range(min_d, max_d):
                    heading = minute + f'Depth {d}'
                    minute = ''
                    print_world(world[d], size, self.output_filename,
                                heading=heading, append=True)

        return sum(sum(v) for v in world.values())


def print_world(world: list[point], size, filename,
                heading=None, append=False):
    if not isdebug:
        return
    with open(filename, 'a+' if append else 'w+') as f:
        if heading:
            f.write(heading + '\n')
        for i, v in enumerate(world, start=1):
            f.write('#' if v else '.')
            if i % size == 0:
                f.write('\n')
        f.write('\n')


def get_edge_maps(size):
    mid = size // 2
    inner = {
            mid*5 + mid-1: [x*size for x in range(size)],
            (mid-1)*5 + mid: [y for y in range(size)],
            (mid+1)*5 + mid: [(size-1)*size + y for y in range(size)],
            mid*5 + mid+1: [x*size + size-1 for x in range(size)],
        }

    outer = {}
    for k, v in inner.items():
        for p in v:
            outer.setdefault(p, []).append(k)

    return inner, outer


def has_bug(bug, adj):
    return int(adj == 1 or (not bug and adj == 2))


def adjacent(world, point, size):
    adj = [p for n in (size, -size) if 0 <= (p := point+n) < size*size]
    adj += [p for n in (1, -1) if (p := point+n)//size == point//size]
    return sum(world[n] for n in adj)


if __name__ == "__main__":
    DaySolution(day_number).run()
