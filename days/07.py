from days import aoc, day
from utils.decorators import time_it
from itertools import permutations
from utils.opcodes import machine

day_number = 7


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]
        pass

    @time_it
    def part1(self, input_data):
        best_signal = 0

        for inputs in permutations(range(5), 5):
            signal = 0

            for i in range(5):
                signal = next(machine(self.state)
                              .add_input(inputs[i])
                              .add_input(signal)
                              .run())

            best_signal = signal if signal > best_signal else best_signal

        return best_signal

    @time_it
    def part2(self, input_data):
        best_signal = 0

        for inputs in permutations(range(5, 10), 5):
            amps = [machine(self.state) for _ in range(5)]

            for i in range(5):
                amps[i].add_input(inputs[i])

            signal = 0

            while not amps[-1].is_halted():
                for amp in amps:
                    amp.add_input(signal)

                    signal = next(amp.run(), signal)

            best_signal = signal if signal > best_signal else best_signal

        return best_signal


if __name__ == "__main__":
    DaySolution(day_number).run()
