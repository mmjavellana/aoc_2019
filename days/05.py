from days import aoc, day
from utils.decorators import time_it
from utils.opcodes import machine

day_number = 5


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = [int(x) for x in input_data[0].split(',')]

    @time_it
    def part1(self, input_data):
        return compute(self.state, 1)

    @time_it
    def part2(self, input_data):
        return compute(self.state, 5)


def compute(state: list, input):
    return list(machine(state)
                .add_input(input)
                .run())


if __name__ == "__main__":
    DaySolution(day_number).run()
